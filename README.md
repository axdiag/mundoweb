Intrucciones de instalación por comando en consola

Se debe lanzar la API "mundoAPI" antes de comenzar con estos pasos

1- Clonar el repositorio
git clone https://gitlab.com/axdiag/mundoweb.git

2-Agregar react-scripts a las dependencias con el comando en consola: 
npm install react-scripts --save

3-Lanzar la aplicación con el comando: 
npm start 
