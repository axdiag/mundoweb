import './App.css';
import Dispositivo from './views/Dispositivo';
import IngresarDispositivo from './views/IngresarDispositivo';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Dispositivo/>}/>
          <Route path="/ingresar" element={<IngresarDispositivo/>}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
