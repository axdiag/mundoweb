
import axios from "axios";
import React, { Component, useRef } from "react";
import {Link} from "react-router-dom";


class Dispositivo extends Component
{
    
    state = {
        dispositivos: [],
        cargando: true,
        bodegas: [],
        marcas: [],
        modelos: []
    }

    async componentDidMount()
    {
        const res = await axios.get('http://localhost:8000/api/dispositivos');
        
        if (res.data.status === 200) {
            this.setState({
                dispositivos: res.data.dispositivos,
                cargando: false,
                bodegas: res.data.bodegas,
                marcas: res.data.marcas,
                modelos: res.data.modelos
            })
       
        }
    }

    handleModelo = async (e) => {
        e.preventDefault()

        document.getElementById('filtrobodega').value = '0'
        document.getElementById('filtromarca').value = '0'

        var filtro = await axios.get(`http://localhost:8000/api/filtromodelo/${e.target.value}`)

        if (filtro.data.status === 200) {
            this.setState({
                dispositivos: filtro.data.dispositivos,
            })  
        }
    }

    handleMarca = async (e) => {
        e.preventDefault()
        
        document.getElementById('filtrobodega').value = '0'
        document.getElementById('filtromodelo').value = '0'

        var filtro = await axios.get(`http://localhost:8000/api/filtromarca/${e.target.value}`)

        if (filtro.data.status === 200) {
            this.setState({
                dispositivos: filtro.data.dispositivos,
            })  
        }
    }

    handleBodega = async (e) => {
        e.preventDefault()

        document.getElementById('filtromodelo').value = '0'
        document.getElementById('filtromarca').value = '0'

        var filtro = await axios.get(`http://localhost:8000/api/filtrobodega/${e.target.value}`)
        
        if (filtro.data.status === 200) {
            this.setState({
                dispositivos: filtro.data.dispositivos,
            })  
        }
    }


    render()
    {
        
        var HTML_dispositivos = ''
        if (this.state.cargando) 
        {
            HTML_dispositivos = <tr><td colSpan="5"><h2>Cargando...</h2></td></tr>
        }
        else
        {
            HTML_dispositivos =
            this.state.dispositivos.map( (item) => {
                return (
                    <tr key={item.id}>
                        <td>{item.id}</td>
                        <td>{item.dispositivo}</td>
                        <td>{item.marca}</td>
                        <td>{item.modelo} </td>
                        <td>{item.bodega}</td>
                    </tr>
                );
            })
        }

        return (
            
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">
                                <p> Dispositivos
                                <Link to={"/ingresar"} className="btn btn-primary btn-sm float-end"> Agregar Dispositivo</Link>
                                </p>
                            </div> 
                            <div className="card-body">

                                <div className="d-flex">
                                    <div className="col-md-4">
                                        <span>Bodega</span>
                                        <select  id="filtrobodega" className="btn mx-2" onChange={this.handleBodega}>
                                            <option value='0'>Todas</option>
                                            {this.state.bodegas.map( item => (
                                                <option  key={item.id} value={item.id}>{item.bodega}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="col-md-4">
                                        <span>Marca</span>
                                        <select id="filtromarca" className="btn mx-2" onChange={this.handleMarca}>
                                            <option value='0'>Todas</option>
                                            {this.state.marcas.map( item => (
                                                <option  key={item.id} value={item.id}>{item.marca}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div>
                                        <span>Modelo</span>
                                        <select id="filtromodelo" className="btn mx-2" onChange={this.handleModelo}>
                                            <option value='0'>Todas</option>
                                            {this.state.modelos.map( item => (
                                                <option  key={item.id} value={item.id}>{item.modelo}</option>
                                            ))}
                                        </select>
                                    </div>  
                                </div>

                                <table className="table table-boardered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Bodega</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {HTML_dispositivos}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        );
    }
}

export default Dispositivo