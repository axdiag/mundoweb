import axios from "axios";
import React, { Component } from "react";
import {Link} from 'react-router-dom';
import swal from "sweetalert";


class IngresarDispositivo extends Component
{

    state = {
        dispositivo: '',
        modelo: '',
        marca: '',
        bodega: '',
        modelos: [],
        marcas: [],
        bodegas: [],
        error_message: ''
    }

    handleInput = (e) => {
        e.preventDefault()
        this.setState({
            [e.target.name]: e.target.value
        });

    }

    handleModelos = async (e) => {
        e.preventDefault()

        const id_marca = e.target.value
        const modelos = await axios.get(`http://localhost:8000/api/modelos/${id_marca}`) 

        console.log(modelos.data.modelos);
        this.setState({
            modelos: modelos.data.modelos
        })
    }


    saveDispositivo = async (e) => {
        e.preventDefault()

        document.getElementById('ingresarbtn').disabled= true
        document.getElementById('ingresarbtn').innerText= 'Guardando'

        const res = await axios.post('http://localhost:8000/api/ingresar-dispositivo', this.state);

        if (res.data.status === 200) {
            swal("", res.data.message , "success");
            this.setState({
                dispositivo: '',
                modelo: '',
                marca: '',
                bodega: '',
            })
            document.getElementById('ingresarbtn').innerText= 'Ingresar dispositivo'
            document.getElementById('ingresarbtn').disabled= false
        }
        else{
            document.getElementById('ingresarbtn').innerText= 'Ingresar dispositivo'
            document.getElementById('ingresarbtn').disabled= false
            this.setState({
                error_message: res.data.validation_ERR
            })
        }
    }

    async componentDidMount ()
    {
        let res = await fetch('http://localhost:8000/api/bodegas')
        let info = await res.json()
        console.log(info.modelos);
        if (info.status === 200)  {
            this.setState(
                {
                    marcas: info.marcas,
                    bodegas: info.bodegas
                }
            )
    
        }
       
        
    }

    render() 
    {    
        
        return(
            <div className="container d-flex justify-content-center">
                <div className="row">
                    <div >
                        <div className="card">
                            <div className="card-header">
                                <p>
                                <Link to={"/"} className="btn btn-primary btn-sm float-start">Volver</Link>
                                Agregar dispositivos
                                </p>
                            </div> 
                            <div className="card-body">
                                <form onSubmit={this.saveDispositivo}>
                                    <div className="form-group mb-3">
                                        <span className="float-start px-2">Dispositivo</span>
                                        <input type="text" name="dispositivo" onChange={this.handleInput} value={this.state.dispositivo} className="form-control"></input>
                                        <span className="text-danger">{this.state.error_message.dispositivo}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <span className="float-start px-2">Marca</span>
                                        <select  name="marca" onChange={this.handleInput} onClick={this.handleModelos} className="form-control">
                                            <option>Seleccione una marca </option>
                                            {this.state.marcas.map(item => (
                                                <option key={item.id} value={item.id}>
                                                    {item.marca}
                                                </option>
                                            ))}
                                        </select>
                                        <span className="text-danger">{this.state.error_message.marca}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <span className="float-start px-2">Modelo</span>
                                        <select  name="modelo" onChange={this.handleInput} className="form-control">
                                            <option>Seleccione un modelo </option>
                                            {this.state.modelos.map(item => (
                                                <option key={item.id} value={item.id}>
                                                    {item.modelo}
                                                </option>
                                            ))}
                                        </select>     
                                        <span className="text-danger">{this.state.error_message.modelo}</span>   
                                    </div>

                                    <div className="form-group mb-3">
                                        <span className="float-start px-2">Bodega</span>
                                        <select name="bodega" onChange={this.handleInput} value={this.state.bodega} className="form-control">
                                            <option defaultValue=''>Seleccione una bodega </option>
                                            {this.state.bodegas.map(item => (
                                                <option key={item.id} value={item.id}>
                                                    {item.bodega}
                                                </option>
                                            ))}
                                        </select>
                                        <span className="text-danger">{this.state.error_message.bodega}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <button id='ingresarbtn' className="btn btn-primary btn-sm float-center">Ingresar dispositivo</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        );
    }
}

export default IngresarDispositivo;